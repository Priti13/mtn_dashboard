import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { DashboardService } from '../service/dashboard.service';
import { ButtonRendererComponent } from '../renderer/button-renderer.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pageIndex: number = 1;
  pageSize = 10;
  totalCount: number = 0;
  // ag grid
  columnDefs: any[];
  rowData: any[];
  defaultColDef: any;
  gridApi: any;
  getRowHeight;
  frameworkComponents: any;
  rowDataClicked1 = {};
  showComp = false;
  compName = 'dashboard';
  // side menus
  sideMenu: allTab[]; //{id: number, name: string, parent: number}
  oldId: any;
  oldIdsubmenuid: any;
  showMore: boolean = false;
  type = 'device';
  parent = null;

  // firstTotalCount: number = 0;
  // high_severity: number = 0;
  // medium_severity: number = 0;
  // low_severity: number = 0;
  // mtn_cve: number = 0;
  // mtn_high_severity: number = 0;
  // mtn_low_severity: number = 0;
  // mtn_medium_severity: number = 0;

  // card-view-details
  mtnType: any;
  cardType: any;
  mainMenuActive: boolean = false;

  constructor(
    private dataTableService: DashboardService,
    private spinner: NgxSpinnerService, private router: Router) {
    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    }
    this.columnDefs = [
      { headerName: 'CVE ID', field: 'cve_id', sortable: true, filter: false },
      { headerName: 'Severity', field: 'severity', sortable: true, filter: false },
      { headerName: 'Version', field: 'version', sortable: true, filter: false },
      { headerName: 'Vendor', field: 'vendor', sortable: true, filter: false },
      { headerName: 'Product', field: 'product', sortable: true, filter: false },
      {
        headerName: 'Action',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.onBtnClick1.bind(this),
          label: 'View more'
        }
      },
      // { headerName: 'Button', field: '' }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      flex: 1,
      minWidth: 100,
    };
  }

  ngOnInit() {
    this.sideMenu = [];
    this.rowData = [];
    this.getTableData('device', this.parent, 1);
  }

  // onGridReady(params) {
  //   this.gridApi = params.api;
  //   // this.gridColumnApi = params.columnApi;
  // }
  // get all data on page load
  getTableData(type, parent, page) { // type, parent,page
    this.spinner.show();
    this.rowData = [];
    this.showComp = false;
    this.dataTableService.getTableDataDetails(type, parent, page).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.rowData = res['result'];
        this.totalCount = res['count'];
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      console.log(err.message);
    });
  }
  // pagination
  pageChanged(event) {
    // update current page of items
    this.pageIndex = event;
    this.getTableData(this.type, this.parent, this.pageIndex);
  }
  // get side menu on page load
  getSideMenuData() {
    this.mainMenuActive = !this.mainMenuActive;
    this.showComp = false;
    this.compName = 'dashboard';
    this.spinner.show();
    this.type = 'device';
    this.pageIndex = 1;
    this.parent = null;
    this.dataTableService.sideMenuDetails().subscribe(res => {
      if (res) {
        this.spinner.hide();
        // this.getTableData(this.type, this.parent, this.pageIndex);
        this.sideMenu = res as any[];
        this.sideMenu.map(x => {
          x['isActive'] = false
          x['subMenu'] = []
        }
        );
        console.log(this.sideMenu);
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      console.log(err.message);
    });
  }
  getSubMenu(id: number, type) {
    this.showComp = false;
    this.compName = 'dashboard';
    if (this.oldId !== id) {
      this.oldId = id;
      this.sideMenu.forEach(el => {
        if (el.id !== id) {
          el.subMenu = [];
          el.isActive = false;
        } else {
          el.isActive = true;
        }
      });
      this.type = type;
      this.parent = id;
      this.pageIndex = 1;
      this.dataTableService.sideMenuDetails(id).subscribe(res => {
        if (res) {
          this.spinner.hide();
          this.getTableData(type, this.parent, this.pageIndex);
          this.sideMenu.find(el => el.id === id).subMenu = res as any[];
          this.sideMenu.find(el => el.id === id).subMenu.map(x => {
            x['isActive'] = false
            x['subMenu'] = []
          });
        } else {
          this.spinner.hide();
        }
      }, err => {
        this.spinner.hide();
        console.log(err.message);
      });
    }
  }

  getSubOfSubMenu(menuid: number, submenuid: number, type) {
    this.showComp = false;
    this.compName = 'dashboard';
    if (this.oldId !== submenuid) {
      this.oldId = submenuid;
      this.sideMenu.find(el => el.id === menuid).subMenu.forEach(el => {
        if (el.id !== submenuid) {
          el.subMenu = [];
          el.isActive = false;
        } else {
          el.isActive = true;
        }
      });
      this.type = type;
      this.parent = submenuid;
      this.pageIndex = 1;
      this.dataTableService.sideMenuDetails(submenuid).subscribe(res => {
        if (res) {
          this.spinner.hide();
          this.getTableData(type, this.parent, this.pageIndex);
          this.sideMenu.find(el => el.id === menuid).subMenu.find(x => x.id === submenuid).subMenu = res as any[];
        } else {
          this.spinner.hide();
        }
      }, err => {
        this.spinner.hide();
        console.log(err.message);
      });
    }
  }

  getlastMenu(childmenu: number, type) {
    this.showComp = false;
    this.compName = 'dashboard';
    this.type = type;
    this.parent = childmenu;
    this.pageIndex = 1;
    this.totalCount = 0;
    this.getTableData(type, this.parent, this.pageIndex);
  }

  geTotalCount() {
    return this.totalCount;
  }
  onBtnClick1(e) {
    this.showComp = true;
    this.compName = 'view-more';
    this.rowDataClicked1 = e.rowData;
  }
  // get all components
  getcomponent() {
    if (this.showComp) {
      this.showComp = false;
      this.compName = 'dashboard';
    } else {
      this.showComp = true;
      // return this.showComp;
    }
  }
  // // navigate to next details page
  displayComponents(event) {
    this.compName = 'card-view';
    this.showComp = true;
    this.mtnType = event.mtnType;
    this.cardType = event.cardType;
  }
 
  // getStatus(){
  //   this.mainMenuActive = !this.mainMenuActive;
  //    return this.mainMenuActive;
  // }
}

interface allTab {
  id: number; name: string; parent: number; isActive: boolean; subMenu: allTab[];
}