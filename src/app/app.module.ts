import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { ButtonRendererComponent } from './renderer/button-renderer.component';
import { DashboardViewMoreComponent } from './dashboard-view-more/dashboard-view-more.component';
import { HomeComponent } from './home/home.component';
import { CardViewDetailsComponent } from './card-view-details/card-view-details.component';
import { CardTableDetailsComponent } from './card-table-details/card-table-details.component';
import { LoginComponent } from './login/login.component';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'login', component: LoginComponent},
  { path: 'view-details', component: CardViewDetailsComponent},
  // { path: 'view-details', component: CardViewDetailsComponent},
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ButtonRendererComponent,
    DashboardViewMoreComponent,
    HomeComponent,
    CardViewDetailsComponent,
    CardTableDetailsComponent,
    LoginComponent,
    DashboardCardComponent
  ],
  entryComponents: [ButtonRendererComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    RouterModule.forRoot(routes),
    AgGridModule.withComponents([ButtonRendererComponent])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
