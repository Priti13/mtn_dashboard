import { Component, OnInit, Input } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { DashboardService } from '../service/dashboard.service';
import { Router } from '@angular/router';
import { ButtonRendererComponent } from '../renderer/button-renderer.component';

@Component({
  selector: 'app-card-view-details',
  templateUrl: './card-view-details.component.html',
  styleUrls: ['./card-view-details.component.css']
})
export class CardViewDetailsComponent implements OnInit {

  cardData: any[];
  @Input() cardType: any;
  @Input() mtnType: any;

  // 
  showTemp = false;
  tableHeaderName: string;
  productName: string;
  vendorName: string;
  version: string;
  // tableData: any[];
  pageIndex: number = 1;
  pageSize = 10;
  totalCount: number = 0;
  // ag grid
  columnDefs: any[];
  rowData: any[];
  defaultColDef: any;
  gridApi: any;
  getRowHeight;
  frameworkComponents: any;
  rowDataClicked1 = {};
  compName = 'info-view';
  constructor(private dataTableService: DashboardService,
    private spinner: NgxSpinnerService, private router: Router) {
    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    }
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      flex: 1,
      minWidth: 100,
    };
  }

  ngOnInit() {
    console.log(this.mtnType, this.cardType);
   this.initialiseTableHeader();
    this.getTableData();
  }

  initialiseTableHeader(){
    this.formatHeaderName();
    this.columnDefs = [
      { headerName: 'Product Name', field: 'product', sortable: true, filter: false },
      { headerName: 'Vendor Name', field: 'vendor', sortable: true, filter: false },
      { headerName: 'Version', field: 'version', sortable: true, filter: false },
      { headerName: this.tableHeaderName, field: 'product', sortable: true, filter: false },
      {
        headerName: 'Action',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.onBtnClick1.bind(this),
          label: 'View more'
        }
      },
    ];
  }
  // get table data according to mtn type and cardtype
  getTableData() {
    this.spinner.show();
    this.rowData = [];
    this.dataTableService.cardDetails(this.mtnType, this.cardType, this.pageIndex).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.rowData = res['result'];
        this.totalCount = res['count'];
        console.log(this.rowData);
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      console.log(err.message);
    });
  }
  // pagination
  pageChanged(event) {
    this.pageIndex = event;
    this.getTableData();
  }
  // click on view more button on info table
  onBtnClick1(e) {
    this.showTemp = true;
    this.compName = 'cve-view';
    this.productName = e.rowData.product;
    this.vendorName = e.rowData.vendor;
    this.version = e.rowData.version;
    // this.rowDataClicked1 = e.rowData;
  }

  // click to any card call new values
  newtableValue(event) {
    // if(this.mtnType !== event.mtnType && this.cardType !== event.cardType){
    this.compName = 'info-view';
    this.showTemp = false;
    this.mtnType = event.mtnType;
    this.cardType = event.cardType;
    console.log(this.mtnType, this.cardType);
    this.initialiseTableHeader();
    this.getTableData();
    this.productName = '';
    this.vendorName = '';
    this.version = '';
    // }
  }
  // gotoCardTable(obj) {
  //   this.showTemp = true;
  //   this.productName = obj.product;
  //   this.vendorName = obj.vendor;
  //   this.version = obj.version;
  // }
  formatHeaderName() {
    this.tableHeaderName = '';
    if (this.mtnType === 'total') {
      this.tableHeaderName = 'Total';
    } else if (this.mtnType === 'mtn') {
      this.tableHeaderName = 'Mtn';
    }
    if (this.cardType === 'high_severity') {
      this.tableHeaderName = this.tableHeaderName + ' ' + 'High Severity'
    } else if (this.cardType === 'medium_severity') {
      this.tableHeaderName = this.tableHeaderName + ' ' + 'Medium Severity'
    } else if (this.cardType === 'low_severity') {
      this.tableHeaderName = this.tableHeaderName + ' ' + 'Low Severity'
    } else if (this.cardType === 'cve') {
      this.tableHeaderName = this.tableHeaderName + ' ' + 'CVE'
    }
  }
}
