import { Component, OnInit, Input } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { DashboardService } from '../service/dashboard.service';
import { ButtonRendererComponent } from '../renderer/button-renderer.component';

@Component({
  selector: 'app-card-table-details',
  templateUrl: './card-table-details.component.html',
  styleUrls: ['./card-table-details.component.css']
})
export class CardTableDetailsComponent implements OnInit {
  @Input() productName: any;
  @Input() vendorName: any;
  @Input() version: any;
  @Input() mtnType: any;
   @Input() cardType: any;

  // tableData: any[];
  pageIndex: number = 1;
  pageSize = 10;
  totalCount: number = 0;
  // ag grid
  columnDefs: any[];
  rowData: any[];
  defaultColDef: any;
  gridApi: any;
  getRowHeight;
  frameworkComponents: any;
  rowDataClicked1 = {};
  // components value
  showComp = false;
  compName = 'card';

  constructor(private dataTableService: DashboardService,
    private spinner: NgxSpinnerService, ) {
    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    }
    this.columnDefs = [
      { headerName: 'CVE ID', field: 'cve_id', sortable: true, filter: false },
      { headerName: 'Severity', field: 'severity', sortable: true, filter: false },
      { headerName: 'Version', field: 'version', sortable: true, filter: false },
      { headerName: 'Vendor', field: 'vendor', sortable: true, filter: false },
      { headerName: 'Product', field: 'product', sortable: true, filter: false },
      {
        headerName: 'Action',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.onBtnClick1.bind(this),
          label: 'View more'
        }
      },
      // { headerName: 'Button', field: '' }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      flex: 1,
      minWidth: 100,
    };
  }

  ngOnInit() {
    this.getTableData();
  }
  getTableData() {
    this.spinner.show();
    this.rowData = [];
    this.showComp = false;
    this.dataTableService.cardTableDetails(this.productName, this.vendorName, this.version, this.mtnType, this.cardType, this.pageIndex).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.rowData = res['result'];
        this.totalCount = res['count'];
        console.log(this.totalCount, this.rowData);
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      console.log(err.message);
    });
  }
  // pagination
  pageChanged(event) {
    // update current page of items
    this.pageIndex = event;
    this.getTableData();
  }
  // mtn card
  tableValueChange(event) {
    // this.compName = 'card-view';
    // this.showTemp = false;
    this.mtnType = event.mtnType;
    this.cardType = event.cardType;
    console.log(this.mtnType, this.cardType);
   this.getTableData();
  }
  onBtnClick1(e) {
    this.showComp = true;
    this.compName = 'view';
    this.rowDataClicked1 = e.rowData;
  }
  // get all components
  getcomponent() {
    if (this.showComp) {
      this.showComp = false;
      this.compName = 'card';
    } else {
      this.showComp = true;
      // return this.showComp;
    }
  }
}
