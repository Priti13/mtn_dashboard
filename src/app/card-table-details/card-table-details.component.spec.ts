import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTableDetailsComponent } from './card-table-details.component';

describe('CardTableDetailsComponent', () => {
  let component: CardTableDetailsComponent;
  let fixture: ComponentFixture<CardTableDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTableDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTableDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
