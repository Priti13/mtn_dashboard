import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError, filter } from 'rxjs/operators';
// import { User, CurrentUser, RefreshUser } from '../_model/user';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private headers: HttpHeaders = new HttpHeaders();
  // private currentUserSubject: BehaviorSubject<CurrentUser>;
  // public currentUser: Observable<CurrentUser>;

  constructor(private http: HttpClient) {
    // this.currentUserSubject = new BehaviorSubject<CurrentUser>
    //   (JSON.parse(localStorage.getItem('currentUser')));
    // this.currentUser = this.currentUserSubject.asObservable();
  }

  // login(obj: User): Observable<any> {
  //   const formData: FormData = new FormData();
  //   for (const key in obj) {
  //     if (obj.hasOwnProperty(key)) {
  //       formData.append(key, obj[key]);
  //     }
  //   }
  //   return this.http.post<any>(environment.url + 'oauth/token', formData).pipe(
  //     map(userData => {
  //       let currentUser: CurrentUser = new CurrentUser();
  //       currentUser.type = userData['token_type'];
  //       currentUser.token = userData['access_token'];
  //       currentUser.refresh = userData['refresh_token'];

  //       this.setCurrentUser(currentUser);
  //       return true;
  //     }),
  //     filter(userData => userData === true));
  // }

  login(username: string, password: string) {
    return this.http.post<any>(`/users/authenticate`, { username: username, password: password }, { headers: this.headers })
        .pipe(map(user => {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
            }

            return user;
        }));
}

  errorHandler(respError: HttpErrorResponse) {
    if (respError.error instanceof ErrorEvent) {
      console.error('Client Side Error: ' + respError);
    } else {
      console.error('Server Side Error: ' + respError);
    }

    return throwError(respError || 'Server Error');
  }
  // public get currentUserValue(): CurrentUser {
  //   return this.currentUserSubject.value;
  // }
  // public setCurrentUser(currentUser: CurrentUser): boolean {
  //   localStorage.setItem('currentUser', JSON.stringify(currentUser));
  //   this.currentUserSubject.next(currentUser);
  //   return true;
  // }
  // public removeCurrentUser(): boolean {
  //   localStorage.removeItem('currentUser');
  //   this.currentUserSubject.next(JSON.parse(localStorage.getItem('currentUser')));
  //   this.router.navigate(['/'])
  //   return true;
  // }
}
