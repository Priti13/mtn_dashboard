import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getTableDataDetails(type, parent,page) {
    const url = environment.url + `nve_data/?type=${type}&parent=${parent}&page_num=${page}`;
    return this.http.get(url);
  }
  // data-tables/employee-data?skip=${page}&limit=${pageLimit}

  sideMenuDetails(id?) {
    const url = environment.url + `device_data/?parent=${id !== undefined ? id : ''}`;
    return this.http.get(url);
  }
  // card-view-details page api
  cardDetails(mtn_type, card_type,pageIndex) {
    const url = environment.url + `cards_data?page_num=${pageIndex}&mtn_type=${mtn_type}&card_type=${card_type}`;
    return this.http.get(url);
  }
    // card-view-table page api
    cardTableDetails(productName, vendorName, version, mtnType, cardType, pageIndex) {
      const url = environment.url + `cards_details?page_num=${pageIndex}&product=${productName}&vendor=${vendorName}&version=${version}&mtn_type=${mtnType}&card_type=${cardType}`;
      return this.http.get(url);
    }
}
