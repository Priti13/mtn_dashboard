import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardViewMoreComponent } from './dashboard-view-more.component';

describe('DashboardViewMoreComponent', () => {
  let component: DashboardViewMoreComponent;
  let fixture: ComponentFixture<DashboardViewMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardViewMoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardViewMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
