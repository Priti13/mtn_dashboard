import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-view-more',
  templateUrl: './dashboard-view-more.component.html',
  styleUrls: ['./dashboard-view-more.component.css']
})
export class DashboardViewMoreComponent implements OnInit {

  @Input() rowDataClicked1;
  viewData: any;
  constructor() { }

  ngOnInit() {
  this.viewData = this.rowDataClicked1;
  }
}
