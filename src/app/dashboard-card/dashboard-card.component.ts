import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DashboardService } from '../service/dashboard.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.css']
})
export class DashboardCardComponent implements OnInit {

  parent = null;
  compName: string;
  mtnType: any;
  cardType: any;
  showComp: boolean;
  // @Input() dashboardCardValue: any;
  @Output() valueChange = new EventEmitter();
  dashboardCardValue: { firstTotalCount: any; high_severity: any; medium_severity: any; low_severity: any; mtn_cve: any; mtn_high_severity: any; mtn_low_severity: any; mtn_medium_severity: any; };

  constructor(private dataTableService: DashboardService,
    private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    const newValue = JSON.parse(sessionStorage.getItem('cardValues'));
    console.log(newValue);
    if(newValue === null){
      this.getTableDataOnPageLoad();
    } else{
      this.dashboardCardValue = newValue;
    }

  }
  getTableDataOnPageLoad() {
    this.spinner.show();
    this.dataTableService.getTableDataDetails('device', this.parent, 1).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.dashboardCardValue = {
          'firstTotalCount': res['count'],
          'high_severity': res['high_severity'],
          'medium_severity': res['medium_severity'],
          'low_severity': res['low_severity'],
          'mtn_cve': res['mtn_cve'],
          'mtn_high_severity': res['mtn_high_severity'],
          'mtn_low_severity': res['mtn_low_severity'],
          'mtn_medium_severity': res['mtn_medium_severity']
        }
        sessionStorage.setItem('cardValues', JSON.stringify(this.dashboardCardValue));
        // this.firstTotalCount = res['count'];
        // this.high_severity = res['high_severity'];
        // this.medium_severity = res['medium_severity'];
        // this.low_severity = res['low_severity'];
        // this.mtn_cve = res['mtn_cve'];
        // this.mtn_high_severity = res['mtn_high_severity'];
        // this.mtn_low_severity = res['mtn_low_severity'];
        // this.mtn_medium_severity = res['mtn_medium_severity'];
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      console.log(err.message);
    });
  }

  // navigate to next details page
  gotoCardDetailsTotal(mtn_type, card_type) {
    this.valueChange.emit({ mtnType: mtn_type, cardType: card_type });
  }
}
