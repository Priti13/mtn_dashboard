import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 loginForm: {userName: string, password: string};
  errorMsg: string;
  loginCredentials: { userName: string; password: string; }[];
  constructor(private router: Router,private spinner: NgxSpinnerService,) { }

  ngOnInit() {
    this.loginForm = {userName: '', password: ''};
  }
  submitLogin() {
    this.errorMsg = '';
  if(this.loginForm.userName === 'admin' && this.loginForm.password === 'admin123'){
    this.router.navigate(['/dashboard']);
  } else if(this.loginForm.userName === 'Deepinder' && this.loginForm.password === 'Deepinder@123'){
    this.router.navigate(['/dashboard']);
  } else{
    this.errorMsg = 'Wrong username or password.';
    this.router.navigate(['/login']);
  }


    this.loginCredentials = [
      {userName: 'admin', password: 'admin'},
      {userName: 'admin1', password: 'admin1234'},
      {userName: 'admin2', password: 'admin1235'}
    ];

    console.log(this.loginForm);
    //  for(let i=0; i<this.loginCredentials.length; i++){
    //   if(this.loginForm.userName == this.loginCredentials[i].userName && this.loginForm.password == this.loginCredentials[i].password){
    //     this.errorMsg = '';
    //     this.router.navigate(['/dashboard']);
    //   } else{
    //     this.errorMsg = 'Wrong username or password.';
    //     this.router.navigate(['/login']);
    //   }
    //  }


  // if(this.loginForm.userName === 'admin' && this.loginForm.password === 'admin123'){
  //   this.router.navigate(['/dashboard']);
  // } else{
  //   this.router.navigate(['/login']);
  // }
    // this.spinner.show();
    // this.authenticationService.userNewLogin(this.loginForm).subscribe(resData => {
    //   if (resData['status'] === true) {
    //     const userRole = JSON.parse(localStorage.getItem('currentUser')).role;
    //     if (userRole === 'super_admin') {
    //       this.router.navigate(['/modules/dashboard']);
    //     } else if (userRole === 'hr_user') {
    //       // this.router.navigate(['/modules/employee-master-data']);
    //       this.router.navigate(['/modules/dashboard']);
    //     } else if (userRole === 'user') {
    //       this.router.navigate(['/modules/user-dashboard']);
    //     }
    //   } else {
    //     this.spinner.hide();
    //     this.toastr.error(resData['msg']);
    //   }
    // }, () => this.spinner.hide());

  }

}
